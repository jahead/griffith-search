const webpack = require('webpack'); // eslint-disable-line import/no-extraneous-dependencies
const ExtractTextPlugin = require('extract-text-webpack-plugin'); // eslint-disable-line import/no-extraneous-dependencies
const HtmlWebpackPlugin = require('html-webpack-plugin'); // eslint-disable-line import/no-extraneous-dependencies
const HtmlWebpackPrefixPlugin = require('html-webpack-prefix-plugin'); // eslint-disable-line import/no-extraneous-dependencies

exports.devServer = function devServer(options) {
  return {
    // configuation for the webpack-dev-server plugin
    devServer: {
      // defaults to localhost
      host: options.host,
      // defaults to 8080
      port: options.port,
      // remove browser status bar when running in production
      inline: false,
      // display erros only in console to limit webpack output size
      stats: 'errors-only',

      historyApiFallback: true,
      // respond to 404s with index.html

      hot: false,
      // enable HMR on the server
    },
  };
};

exports.POLYFILLS = function POLYFILLS() {
  return {
    entry: [
      'babel-polyfill',
    ],
  };
};

exports.HMR = function HMR(host, port) {
  return {
    entry: [
      'react-hot-loader/patch',
      // activate HMR for React

      `webpack-dev-server/client?http://${host}:${port}`,
      // bundle the client for webpack-dev-server
      // and connect to the provided endpoint

      'webpack/hot/only-dev-server',
      // bundle the client for hot reloading
      // only- means to only hot reload for successful updates

    ],
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
    ],
  };
};

exports.HTML = function HTML(path, prefix) {
  return {
    plugins: [
      new HtmlWebpackPlugin({
        template: path,
        prefix,
      }),
      new HtmlWebpackPrefixPlugin(),
    ],
  };
};

exports.OPTIMIZE = function OPTIMIZE() {
  return {
    plugins: [
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false,
        },
      }),
    ],
  };
};

exports.VIDEOS = function VIDEOS() {
  return {
    module: {
      rules: [
        {
          test: /\.(mp4)$/i,
          loaders: [
            {
              loader: 'file-loader',
              query: {
                name: 'videos/[hash].[ext]',
              },
            },
          ],
        },
      ],
    },
  };
};

exports.IMGS = function IMGS() {
  return {
    module: {
      rules: [
        {
          test: /\.(gif|png|jpe?g|svg)$/i,
          loaders: [
            {
              loader: 'file-loader',
              query: {
                name: 'imgs/[hash].[ext]',
              },
            },
            {
              loader: 'image-webpack-loader',
              query: {
                progressive: true,
                optimizationLevel: 7,
                interlaced: false,
                pngquant: {
                  quality: '65-90',
                  speed: 4,
                },
              },
            },
          ],
        },
      ],
    },
  };
};

exports.lintJS = function lintJS({ paths, options }) {
  return {
    // this module is merged with the babel-loader module in the commons object via
    // `webpack-merge`
    module: {
      rules: [
        {
          test: /\.jsx?$/,
          include: paths,
          exclude: /node_modules/,
          // enables ESLint to run before anything else
          enforce: 'pre',
          use: [
            // eslint runs before babel
            {
              loader: 'eslint-loader',
              options,
            },
          ],
        },
      ],
    },
  };
};

exports.DEBUG = function DEBUG(env) {
  if (env === 'production') {
    return {
      plugins: [
        new webpack.ProvidePlugin({
          'global.DEBUG': false,
        }),
      ],
    };
  }
  return {
    plugins: [
      new webpack.ProvidePlugin({
        'global.DEBUG': true,
      }),
    ],
  };
};

exports.CSS = function CSS(env) {
  // In production, extract CSS into a separate file depending and inject
  // into the head of the document
  if (env === 'production') {
    return {
      module: {
        rules: [
          {
            test: /^((?!\.global).)*\.css$/,
            use: ExtractTextPlugin.extract({
              use: {
                loader: 'css-loader',
                options: {
                  sourceMap: true,
                  modules: true,
                  localIdentName: '[path][name]__[local]--[hash:base64:5]',
                  exclude: /bootstrap/,
                },
              },
            }),
          },
          {
            test: /\.global\.css$/,
            use: ExtractTextPlugin.extract({
              use: {
                loader: 'css-loader',
                options: {
                  sourceMap: true,
                  modules: false,
                  importLoaders: true,
                },
              },
            }),
          },
        ],
      },
      plugins: [
        new ExtractTextPlugin({
          filename: 'css/[name].css',
          allChunks: true,
        }),
      ],
    };
  }

  return {
    module: {
      rules: [
        {
          test: /\.global\.css$/,
          use: [
            {
              loader: 'style-loader',
            },
            {
              loader: 'css-loader',
              options: {
                sourceMap: true,
                include: /bootstrap/,
                modules: false,
              },
            },
          ],
        },
        {
          // regex pattern that matches any CSS files
          test: /^((?!\.global).)*\.css$/,
          use: [
            // injects styles into the Document as a <link>
            {
              loader: 'style-loader',
            },
            {
              // applies necessary transformations to CSS files
              loader: 'css-loader',
              options: {
                sourceMap: true,
                // enables CSS modules
                modules: true,
                // generates a unique css rule for component styles. This property is what allows
                // CSS modules to contain rules locally. You can name a CSS rule something generic
                // such as `.normal` or `.red`, and `localIdentName` will generate a unique CSS rule
                // to avoid namespace clashing
                localIdentName: '[path][name]__[local]--[hash:base64:5]',
                exclude: /bootstrap/,
              },
            },
          ],
        },
      ],
    },
  };
};
