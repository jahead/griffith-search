import React from 'react';
import { AppContainer } from 'react-hot-loader';
import ReactDOM from 'react-dom';
import './libs';
import Root from './components/Root/Root';

window.handleError = (e) => {
  /*  eslint no-console: ["error", { allow: ["error"] }] */
  console.error(e, e.stack);
};

window.onunhandledrejection = ({ reason }) => {
  window.handleError(reason instanceof Error ? reason : new Error(reason));
};

window.onerror = (msg, url, line, column, e) => {
  window.handleError(e || new Error(msg, url, line));
};

const render = (Component) => {
  try {
    ReactDOM.render(
      <AppContainer>
        <Component />
      </AppContainer>, document.getElementById('root'));
  } catch (e) {
    window.handleError(e);
  }
};

if (module.hot) {
  module.hot.accept('./components/Root/Root', () => {
    render(Root);
  });
}
try {
  render(Root);
} catch (e) {
  window.handleError(e);
}
