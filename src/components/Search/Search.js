import { PureComponent } from 'react';
import SearchTable from '../SearchTable';

export default class Search extends PureComponent {

  constructor(props) {
    super(props);

  }

  commponentDidMount = (newProps) => {
    this.setState({ loading: true });
    fetch(`api/search/${newProps.params}`).then(data => this.setState({data}));
  }

  commponentDidUpdate = (newProps) => fetch(`api/search/${newProps.params}`).then(data => this.setState({data}));
  render = () => (<SearchTable items={this.data} loading={this.loading} />); 
};
