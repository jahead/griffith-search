import React from 'react';
import SearchTableItem from '../SearchTableItem/SearchTableItem';

export default SearchTable = ({ items }) => (
  <div>
    {
      items.map(i => <SearchTableItem key={i.id} data={i} />)
    }
  </div>
);
