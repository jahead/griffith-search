import React from 'react';
import { Route, BrowserRouter } from 'react-router-dom';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { deepOrange500 } from 'material-ui/styles/colors';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Grid, Row, Col } from 'react-bootstrap';
import Result from '../Result/Result';
import Results from '../Results/Results';
import Search from '../Search/Search';

// import css from './Root.css';

const muiTheme = getMuiTheme({
  palette: {
    accent1Color: deepOrange500,
  },
});

const Root = () => (
  <BrowserRouter >
    <MuiThemeProvider muiTheme={muiTheme}>
      <Route path="/" component={Search} />
      <Route path="/search/:query" component={Results} />
      <Route path="/select/:id" component={Result} />
    </MuiThemeProvider>
  </BrowserRouter>
);

export default Root;
