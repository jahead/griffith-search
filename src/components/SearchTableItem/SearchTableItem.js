import React from 'react';
import { Link } from 'react-router';

const Item = ({ data }) => (<Link to={`select/${data.id}`} />);

export default Item;
